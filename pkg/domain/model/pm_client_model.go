package model

// PMClient struct
type PMClient struct {
	Client pmClientIF
}

type pmClientIF interface {
	GetTicketByFilter(filterID string) *[]PMTicket
	CommentTicketAssignee(msg string, issues *[]PMTicket)
}

// PMClientConf is information
type PMClientConf struct {
	URL           string
	Email         string
	Password      string
	FilterID      string
	TicketComment string
}

// GetTicketByFilter method resolver
func (c *PMClient) GetTicketByFilter(filterID string) *[]PMTicket {
	return c.Client.GetTicketByFilter(filterID)
}

// CommentTicketAssignee method resolver
func (c *PMClient) CommentTicketAssignee(msg string, issues *[]PMTicket) {
	c.Client.CommentTicketAssignee(msg, issues)
	return
}
