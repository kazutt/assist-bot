package model

// ChatMessage model
type ChatMessage struct {
	Event       string
	ChannelID   string
	PostID      string
	PostUserID  string
	MessageText string
}
