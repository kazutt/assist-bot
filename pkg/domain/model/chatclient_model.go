package model

// ChatClient struct
type ChatClient struct {
	Client chatClientIF
}

type chatClientIF interface {
	Listen() chan *ChatMessage
	Reply(msg string, replyToID string)
	Say(msg string)
}

// ChatClientConf is information
type ChatClientConf struct {
	URL         string
	WSURL       string
	BotUserID   string
	TeamID      string // bot work Team
	TeamName    string
	ChannelID   string
	ChannelName string
	Token       string
	Name        string
	Email       string
	Password    string
}

// Listen method resolver
func (c *ChatClient) Listen() chan *ChatMessage {
	return c.Client.Listen()
}

// Reply method resolver
func (c *ChatClient) Reply(msg string, replyToID string) {
	c.Client.Reply(msg, replyToID)
	return
}

// Say method resolver
func (c *ChatClient) Say(msg string) {
	c.Client.Say(msg)
	return
}
