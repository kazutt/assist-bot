package model

// ChatBot struct
type ChatBot struct {
	Bot      chatBotIF
	*BotConf // nest info
	*ChatClient
	*PMClient
	MessageChannel chan *ChatMessage
	// *GitLabClient ...
}

type chatBotIF interface {
}

// BotConf is bot configuration
type BotConf struct {
	*ChatClientConf
	*PMClientConf
	//*GitLabConf ...
}
