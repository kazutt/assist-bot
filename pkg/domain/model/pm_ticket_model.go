package model

import "time"

// PMTicket model
type PMTicket struct {
	Key           string
	Summary       string
	TypeName      string
	PriorityName  string
	AssigneeEmail string
	IssueURL      string
	DueDate       time.Time
}
