package chatclient

import (
	"os"
	"strings"

	model "gitlab.com/kazutt/assist-bot/pkg/domain/model"

	mattermostModel "github.com/mattermost/mattermost-server/model"
)

// MattermostClient (wrapper)
type MattermostClient struct {
	client   *mattermostModel.Client4
	wsclient *mattermostModel.WebSocketClient
	conf     *model.ChatClientConf
}

// NewMatterMostClient is constructor
func NewMatterMostClient(conf *model.ChatClientConf) *model.ChatClient {

	println("----- MatterMostClient Create -----")

	// API client
	var client *mattermostModel.Client4
	client = mattermostModel.NewAPIv4Client(conf.URL)

	if conf.Email != "" && conf.Password != "" { //user login case
		if user, resp := client.Login(conf.Email, conf.Password); resp.Error != nil {
			println("There was a problem logging into the Mattermost server.  Check your Email and Password.")
			println(resp)
		} else {
			println("user login!")
			conf.BotUserID = user.Id
			println(user.Id)
		}
	} else if conf.Token != "" { // botAccount case
		client.SetOAuthToken(conf.Token)
		println("botAccount login!")
		user, _ := client.GetMe("")
		conf.BotUserID = user.Id
		println("BotUserId: ", conf.BotUserID)
	}

	// WebSocketClient
	wsclient, err := mattermostModel.NewWebSocketClient(conf.WSURL, client.AuthToken)
	if err != nil {
		println("We failed to connect to the web socket")
		println(err.Message)
		println(err.Id)
		println(err.DetailedError)
	}

	// MattermostServer HealthCheck
	if props, resp := client.GetOldClientConfig(""); resp.Error != nil {
		println("There was a problem pinging the Mattermost server.  Are you sure it's running?")
		os.Exit(1)
	} else {
		println("Server detected and is running version " + props["Version"])
	}

	// TeamId Set
	if team, resp := client.GetTeamByName(conf.TeamName, ""); resp.Error != nil {
		println("We failed to get the team '" + conf.TeamName + "'")
		os.Exit(1)
	} else {
		conf.TeamID = team.Id
		println("TeamId: ", conf.TeamID)
	}

	// ChannelId Set
	if rchannel, resp := client.GetChannelByName(conf.ChannelName, conf.TeamID, ""); resp.Error != nil {
		println("We failed to get the channels" + conf.ChannelName)
		os.Exit(1)
	} else {
		conf.ChannelID = rchannel.Id
		println("ChannelId: ", conf.ChannelID)
	}

	retClient := new(MattermostClient)
	retClient.client = client
	retClient.wsclient = wsclient
	retClient.conf = conf

	return &model.ChatClient{Client: retClient}
}

// Listen is botUser information Getter
func (c *MattermostClient) Listen() chan *model.ChatMessage {

	rc := make(chan *model.ChatMessage)

	// WebSocketClient start listen
	c.wsclient.Listen()

	go func() {
		for {
			select {
			case event := <-c.wsclient.EventChannel:
				// If this isn't the debugging channel then lets ingore it
				if event.Broadcast.ChannelId != c.conf.ChannelID {
					continue
				}

				// Only reponded to messaged posted and messaged post edited events
				if event.Event != mattermostModel.WEBSOCKET_EVENT_POSTED && event.Event != mattermostModel.WEBSOCKET_EVENT_POST_EDITED {
					continue
				}

				cm := new(model.ChatMessage)
				cm.Event = event.Event
				cm.ChannelID = event.Broadcast.ChannelId

				post := mattermostModel.PostFromJson(strings.NewReader(event.Data["post"].(string)))
				cm.PostID = post.Id
				cm.PostUserID = post.UserId
				cm.MessageText = post.Message

				rc <- cm
			}
		}
	}()

	return rc
}

// Reply message
func (c *MattermostClient) Reply(msg string, replyToID string) {
	post := &mattermostModel.Post{}
	post.ChannelId = c.conf.ChannelID
	post.Message = msg

	post.RootId = replyToID

	if _, resp := c.client.CreatePost(post); resp.Error != nil {
		println("Failed to reply a message to channel")
		printError(resp.Error)
	}

	return
}

// Say message
func (c *MattermostClient) Say(msg string) {
	post := &mattermostModel.Post{}
	post.ChannelId = c.conf.ChannelID
	post.Message = msg

	if _, resp := c.client.CreatePost(post); resp.Error != nil {
		println("Failed to say a message to channel")
		printError(resp.Error)
	}

	return
}

// internal error method
func printError(err *mattermostModel.AppError) {
	println("\tError Details:")
	println("\t\t" + err.Message)
	println("\t\t" + err.Id)
	println("\t\t" + err.DetailedError)
}
