package chatbot

import (
	"io/ioutil"

	"gitlab.com/kazutt/assist-bot/pkg/domain/model"
	chatclient "gitlab.com/kazutt/assist-bot/pkg/infrastructure/chatclient"
	jiraclient "gitlab.com/kazutt/assist-bot/pkg/infrastructure/jiraclient"
	yaml "gopkg.in/yaml.v2"
)

// MattermostChatBot (ChatBot wrapper)
type MattermostChatBot struct {
	BotConf    *model.BotConf
	ChatClient *chatclient.MattermostClient
}

// NewMatterMostBot is constructor
func NewMatterMostBot(filepath string) *model.ChatBot {

	// Load config.yaml
	buf, err := ioutil.ReadFile(filepath)
	if err != nil {
		panic(err)
	}

	// botConf initialize
	ccConf := &model.ChatClientConf{}
	pmConf := &model.PMClientConf{}
	botConf := &model.BotConf{ChatClientConf: ccConf, PMClientConf: pmConf}

	// Read config.yaml
	err = yaml.Unmarshal(buf, botConf)
	if err != nil {
		panic(err)
	}

	// Bot initialize
	cc := chatclient.NewMatterMostClient(botConf.ChatClientConf)
	pc := jiraclient.NewJiraClient(botConf.PMClientConf)

	return &model.ChatBot{BotConf: botConf, ChatClient: cc, PMClient: pc}
}
