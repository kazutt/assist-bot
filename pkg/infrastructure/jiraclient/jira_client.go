package jiraclient

import (
	"fmt"
	"os"
	"strings"
	"time"

	model "gitlab.com/kazutt/assist-bot/pkg/domain/model"

	jira "gopkg.in/andygrunwald/go-jira.v1"
)

// JiraClient (wrapper)
type JiraClient struct {
	client *jira.Client
	conf   *model.PMClientConf
}

// NewJiraClient is constructor
func NewJiraClient(conf *model.PMClientConf) *model.PMClient {

	println("----- JiraClient Create -----")

	var client *jira.Client
	// API client create
	if conf.Email != "" && conf.Password != "" {
		tp := jira.BasicAuthTransport{
			Username: conf.Email,
			Password: conf.Password,
		}

		var err error
		client, err = jira.NewClient(tp.Client(), conf.URL)
		if err != nil {
			println("Failed to Create JIRAClient")
			os.Exit(1)
		}
		println("JIRAClient created!")

	} else {
		println("JIRAClient Email or Password are required !!")
	}

	retClient := new(JiraClient)
	retClient.client = client
	retClient.conf = conf

	return &model.PMClient{Client: retClient}
}

//GetTicketByFilter(filterID string) *[]PMTicket
//CommentTicket(msg string, ticketID string)

// GetTicketByFilter is Ticket Getter
func (c *JiraClient) GetTicketByFilter(filterID string) *[]model.PMTicket {

	// filter ID Check
	if len(filterID) == 0 {
		println("filterID must set!!")
		return nil
	}

	// initialize
	var issues []jira.Issue

	// appendFunc will append jira issues to []jira.Issue
	appendFunc := func(i jira.Issue) (err error) {
		issues = append(issues, i)
		return err
	}

	// SearchPages will page through results and pass each issue to appendFunc
	// In this example, we'll search for all the issues in the target project
	err := c.client.Issue.SearchPages(fmt.Sprintf(`filter=%s`, strings.TrimSpace(filterID)), nil, appendFunc)
	if err != nil {
		println("Issue SearchPages error!!")
		return nil
	}

	retpmt := []model.PMTicket{}

	for _, issue := range issues {

		pmt := new(model.PMTicket)
		pmt.Key = issue.Key
		pmt.Summary = issue.Fields.Summary
		pmt.TypeName = issue.Fields.Type.Name
		pmt.PriorityName = issue.Fields.Priority.Name
		pmt.AssigneeEmail = issue.Fields.Assignee.EmailAddress
		pmt.IssueURL = fmt.Sprintf("%s/browse/%s", c.conf.URL, issue.Key)
		pmt.DueDate = time.Time(issue.Fields.Duedate)

		retpmt = append(retpmt, *pmt)
	}

	return &retpmt
}

// CommentTicketAssignee is comment for ticket assignee
func (c *JiraClient) CommentTicketAssignee(msg string, issues *[]model.PMTicket) {

	for _, issue := range *issues {
		str := fmt.Sprintf("[~%s]\n%s", issue.AssigneeEmail, msg)

		comment := &jira.Comment{
			Body: str,
		}
		c.client.Issue.AddComment(issue.Key, comment)
	}
}
