package usecase

import (
	"fmt"
	"regexp"
	"time"

	model "gitlab.com/kazutt/assist-bot/pkg/domain/model"
)

// ChatBotService is any client service sample. Must be Using with goroutine.
func ChatBotService(cbot *model.ChatBot) {
	for {
		select {
		case cm := <-cbot.MessageChannel:
			// BotUser msg ingore
			if cm.PostUserID == cbot.BotConf.BotUserID {
				continue
			}

			// debug message
			println("Event: ", cm.Event)
			println("ChannelID: ", cm.ChannelID)
			println("PostUserID: ", cm.PostUserID)
			println("MessageText: ", cm.MessageText)

			// get JIRA ticket (sample)
			if matched, _ := regexp.MatchString(`(?:^|\W)get ticket(?:$|\W)`, cm.MessageText); matched {
				tickets := cbot.PMClient.GetTicketByFilter(cbot.PMClientConf.FilterID)
				replyText := tickets2tabletext(*tickets)
				cbot.ChatClient.Reply(replyText, cm.PostID)
				continue
			}

			// comment JIRA ticket (sample)
			if matched, _ := regexp.MatchString(`(?:^|\W)comment ticket(?:$|\W)`, cm.MessageText); matched {
				tickets := cbot.PMClient.GetTicketByFilter(cbot.PMClientConf.FilterID)

				// comment
				cbot.PMClient.CommentTicketAssignee(cbot.PMClientConf.TicketComment, tickets)

				// confirmation reply
				cbot.ChatClient.Reply("Comment Tickets:\n", cm.PostID)

				replyText := tickets2tabletext(*tickets)
				cbot.ChatClient.Reply(replyText, cm.PostID)
				continue
			}

			// table Reply test (sample)
			if matched, _ := regexp.MatchString(`(?:^|\W)test(?:$|\W)`, cm.MessageText); matched {

				testTicket := model.PMTicket{Key: "999", Summary: "summary1", IssueURL: "xxxxx", DueDate: time.Now()}
				testTickets := []model.PMTicket{testTicket, testTicket, testTicket}
				replyText := tickets2tabletext(testTickets)
				cbot.ChatClient.Reply(replyText, cm.PostID)
				continue
			}

			// Reply (sample)
			if matched, _ := regexp.MatchString(`(?:^|\W)reply(?:$|\W)`, cm.MessageText); matched {
				cbot.ChatClient.Reply(cm.MessageText, cm.PostID)
				continue
			}

			// Say (sample)
			cbot.ChatClient.Say(cm.MessageText)
		}
	}
}

func tickets2tabletext(tickets []model.PMTicket) (tabletext string) {

	tabletext = "|Key|Summary|DueDate|\n"
	tabletext += "|--|--|--|\n"
	for _, ticket := range tickets {
		tabletext += fmt.Sprintf("|[%s](%s)|%s|%s|\n", ticket.Key, ticket.IssueURL, ticket.Summary, time2date(ticket.DueDate))
	}

	return tabletext
}

func time2date(time time.Time) (text string) {
	const dateFormat = "2006/1/2"
	return time.Format(dateFormat)
}
