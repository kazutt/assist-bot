assist-bot
====

Chatbot to help manage your project.

## Description
You can easily use the sample app by following Getting started.

The software has also been created with domain-driven design (especially onion architecture) in mind for the author's practice. Please refer to it when expanding.

## Getting Started
#### 1. Clone this repository

#### 2. Get 3rd party library
```
go get gopkg.in/yaml.v2
go get github.com/mattermost/mattermost-server/model
gopkg.in/andygrunwald/go-jira.v1
```
#### 3. Move to app directory
```
cd assist-bot/cmd/sample-app
```
#### 4. Edit config.yaml in app directory

#### 5. Run
```
go run main.go
```


## Licence

[MIT](LICENSE.md)

## Author

[kazutt](https://gitlab.com/kazutt)