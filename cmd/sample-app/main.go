package main

import (
	chatbot "gitlab.com/kazutt/assist-bot/pkg/infrastructure/chatbot"
	usecase "gitlab.com/kazutt/assist-bot/pkg/usecase"
)

const (
	// BotConfigFilePath is yaml format
	BotConfigFilePath = "./config.yaml"
)

func main() {
	// ChatBotCreate
	println("===== ChatBot Create =====")
	cbot := chatbot.NewMatterMostBot(BotConfigFilePath)

	println("===== ChatListen Start =====")
	cbot.MessageChannel = cbot.ChatClient.Listen()

	// ChatBotService start
	println("===== ChatBotService Start =====")
	go usecase.ChatBotService(cbot)

	// You can block forever with
	select {}

}
